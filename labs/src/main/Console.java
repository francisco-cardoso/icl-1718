package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.ParseException;
import parser.Parser;
import parser.SimpleParser;
import parser.TokenMgrError;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		//SimpleParser parser = new SimpleParser(System.in);
		Parser parser = new Parser(System.in);

		while (true) {
			try {
				//parser.Start();
				//System.out.println("OK!" );
				ASTNode n = parser.Start();
				// n.typecheck();
				System.out.println("Tudo OK e o resultado �: ");
				n.eval(new Environment<>());
				//System.out.println("Tudo OK e o resultado �: " + n.eval());
			} catch (TokenMgrError e) {
				System.out.println("Lexical Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			}
		}
	}

	public static boolean accept(String s) throws ParseException {
		//SimpleParser parser = new SimpleParser(new ByteArrayInputStream(s.getBytes()));
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			return n.eval(new Environment<>()) == value;
			
			
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}


}
