package types;

public class BoolType implements IType {

	public static BoolType singleton = new BoolType();
	
	public boolean equals(Object other){
		return (other instanceof BoolType);
	}
}
