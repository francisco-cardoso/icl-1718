package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import types.BoolType;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class InterpreterTests {

	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, IValue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression, value));
	}
	
	 private void testCaseBool(String expression, boolean value) throws ParseException {
	 	assertTrue(Console.acceptCompare(expression, new BoolValue(value)));		
	 }
	
	// private void testNegativeCaseBool(String expression, boolean value) throws ParseException {
	// 	assertFalse(Console.acceptCompareBool(expression, new BoolValue(value)));
	// }
	
	@Test
	public void test01() throws Exception {
		//testCase("1\n",1);
		//testCase("1+2\n",3);
		//testCase("1-2-3\n",-4);
		testCase("1\n",new IntValue(1));
		testCase("1+2\n",new IntValue(3));
		testCase("1-2-5\n",new IntValue(-6));
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		//testCase("4*2\n",8);
		//testCase("4/2/2\n",1);
		//testCase("-1\n", -1);
		//testCase("-1*3\n",-3);
		//testCase("true\n",(IValue) new BoolType());
		// testCaseBool("false\n",false);
		// testCaseBool("11 < 22\n", true);
		// testCaseBool("11 > 22\n", false);
		// testCaseBool("11 == 22\n", false);
		// testCaseBool("3*5 != 1+2 == true\n", true);
		// testCaseBool("1 == 2 && 3 == 4\n", false);
		// testCaseNegativeBool("1 == 2 || 3 == 4 && xpto \n", true);
		// testCaseNegativeBool("!(1 == 2) && xpto \n", true);
		testCase("4*2\n",new IntValue(8));
		testCase("4/2/2\n",new IntValue(1));
		testCase("-1\n",new IntValue(-1));
		testNegativeCase("(2+3)/5\n",new IntValue(2));
	}
	
	@Test
	public void testClass05() throws Exception {
	
		testCase("decl x = 1 in x+1 end\n", new IntValue(2));
     	
	//	testCase("decl x = 1 in decl y = 2 in x+y end end\n",new IntValue(3));
	//testCase("decl x = decl y = 2 in 2*y end in x*3\n",new IntValue(4));
	//	testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end\n", new IntValue(5));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
