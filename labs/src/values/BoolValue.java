package values;

public class BoolValue implements IValue{
	
	private boolean value;
	
	public BoolValue(boolean b) {
		
		value=b;
		
	}

	public boolean getBool() {
		
		return value;
		
	}
	
	@Override
	public String toString() {
		return Boolean.toString(value);
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof BoolValue && value == ((BoolValue)obj).getBool();
	}
}
