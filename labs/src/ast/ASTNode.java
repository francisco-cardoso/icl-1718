package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;


public interface ASTNode {
	
	
    IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException;
//	IType typecheck(Environment<Value> env) throws TypingException;
	
    IType typecheck() throws TypingException;
	
    void compile(CodeBlock code);
	
    IType getType();

	IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException;


}

