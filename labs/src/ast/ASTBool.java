
package ast;

import values.IValue;
import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import values.BoolValue;

public class ASTBool implements ASTNode {

	boolean val;	
	
	public ASTBool(boolean value) {
		val=value;
	}
	
	@Override
	public IValue eval(IEnvironment <IValue> env) {
		return new BoolValue(val);
	}
	
	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public void compile(CodeBlock code) {
		
		code.emit_bool(val);
		
	}

	@Override
	public IType typecheck() throws TypingException {
		
		return new BoolType();
		
	}

	@Override
	public IType getType() {
		
		return new BoolType();
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		return null;
	}
}
