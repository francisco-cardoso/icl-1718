package ast;

 import compiler.CodeBlock;
 import types.*;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
 import values.IValue;
 
 public class ASTAnd implements ASTNode {
 
 	ASTNode left, right;
 	IType Btype;
 
 	public ASTAnd(ASTNode l, ASTNode r) {
 		left = l;
 		right = r;
 	}
 
 	@Override
 	public String toString() {
 		return left.toString() + " && " + right.toString();
 	}
 
	@Override
	public IValue eval(IEnvironment <IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		
		IValue leftV = left.eval(env);
		IValue rightV = right.eval(env);
		
		if(leftV instanceof BoolValue && rightV instanceof BoolValue) {
			
			return new BoolValue(((BoolValue)leftV).getBool() && ((BoolValue)rightV).getBool());
			
		}
		
		else {
			System.out.println("Wrong arguments (and)");
			return null;
		
	}
}
	@Override
	public IType typecheck() throws TypingException {
		
		IType l = left.typecheck();
		IType r = right.typecheck();
		BoolType type = new BoolType();
		
		if(l == type && r == type) {
			Btype = type;
			return type;
			
		}
		else {
			throw new TypingException();
		}
	}

 	@Override
	public void compile(CodeBlock code) {
		
 		String labelFalse, labelExit;
		
		labelFalse = CodeBlock.labelFactory.getLabel();
		labelExit = CodeBlock.labelFactory.getLabel();
		
		left.compile(code);
				
		code.emit_ifeq(labelFalse);
		
		right.compile(code);		
					
		code.emit_ifeq(labelFalse);

		code.emit_bool(true);
		
		code.emit_jump(labelExit);
		
		code.emit_anchor(labelFalse);

		code.emit_bool(false);
		
		code.emit_anchor(labelExit);
	}

	@Override
	public IType getType() {
		
		return Btype;
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		return null;
	}
 }