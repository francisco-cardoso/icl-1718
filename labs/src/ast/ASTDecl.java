package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTDecl implements ASTNode {
	static class Declaration {
		public Declaration(String id, ASTNode def) {
			this.id = id;
			this.def = def;
		}

		String id;
		ASTNode def;

	}

	ASTNode body;
	ArrayList<Declaration> decls;
	IType type;

	public ASTDecl() {
		decls = new ArrayList<>();
	}

	public void addBody(ASTNode body) {
		this.body = body;
	}

	public void newBinding(String id, ASTNode e) {
		decls.add(new Declaration(id, e));
	}

	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		IEnvironment<IType> newenv = env.beginScope();
		
		for( Declaration d: decls)
			newenv.assoc(d.id, d.def.typecheck(env));
		
		IType type = body.typecheck(newenv);
		
		env.endScope();
		
		return type;	
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO
	}

	@Override
	public IType getType() {

		return type;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		IEnvironment<IValue> newenv = env.beginScope();

		for (Declaration d : decls)
			newenv.assoc(d.id, d.def.eval(env));

		IValue value = body.eval(newenv);

		env.endScope();

		return value;

	}

	@Override
	public IType typecheck() throws TypingException {
		// TODO Auto-generated method stub
		return null;
	}
}