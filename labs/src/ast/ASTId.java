package ast;


import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTId implements ASTNode {
	String id;
	IType type;
	
	public ASTId(String id) {
		this.id = id;
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException {
		
		return env.find(id);
	}

	@Override
	public IType typecheck() throws TypingException {
		return null;	
	}

	@Override
	public void compile(CodeBlock code) {

		code.emit_id();
		
	}

	@Override
	public IType getType() {
		
		return type;
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		return null;
	}
}