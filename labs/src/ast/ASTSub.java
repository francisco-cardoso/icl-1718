package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTSub implements ASTNode {

	ASTNode left, right;
	IType typeInt;

	public ASTSub(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " - " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		
		IValue leftV = left.eval(env);
		IValue rightV = right.eval(env);

			return new  IntValue(((IntValue) leftV).getValue() - ((IntValue) rightV).getValue());

	}

	@Override
	public void compile(CodeBlock code) {
		
		left.compile(code);
		right.compile(code);
		code.emit_sub();
		
	}

	@Override
	public IType typecheck() throws TypingException {

		IValue l = (IValue) left.typecheck();
		IValue r = (IValue) right.typecheck();
		IntType type = new IntType();

		if (l == type && r == type) {
			typeInt = type;
			return typeInt;

		} else {
			throw new TypingException();
		}

	}

	@Override
	public IType getType() {

		return typeInt;

	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		return null;
	}
}

