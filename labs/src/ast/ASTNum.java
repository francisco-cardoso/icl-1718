package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {

	int val;

	public ASTNum(int n) {
		val = n;
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}


	public IValue eval(IEnvironment<IValue> env) {
		return new IntValue(val);
	}

	@Override
	public void compile(CodeBlock code) {
		
		code.emit_push(val);
		
	}

	@Override
	public IType typecheck() throws TypingException {
		
		return new IntType();
		
	}

	@Override
	public IType getType() {
		
		return new IntType();
	
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws DuplicateIdentifierException {
		// TODO Auto-generated method stub
		return null;
	}
}
